package HelperClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

  private final static String HOSTNAME = "den1.mysql6.gear.host";
  private final static String DBNAME = "familybei";
  private final static String USERNAME = "familybei";
  private final static String PASSWORD = "tp%ipd12";

  public Connection connect() {
    Connection conn = null;
    try {
      conn = (Connection) DriverManager.getConnection(
            "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
            USERNAME, PASSWORD);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
    return conn;
  }
}
